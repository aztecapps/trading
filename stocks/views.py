from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from .models import Stock, Trade

class IndexView(generic.ListView):
  template_name = 'stocks/index.html'
  context_object_name = 'stocks_list'

  def get_queryset(self):
    return Stock.objects.order_by('ticker').all()

class DetailView(generic.DetailView):
  model = Stock
  template_name = 'stocks/detail.html'

class ResultsView(generic.DetailView):
  model = Stock
  template_name = 'stocks/results.html'

def vote(request, stock_id):
  stock = get_object_or_404(Stock, pk=stock_id)
  try:
    selected_trade = stock.trade_set.get(pk=request.POST['trade'])
  except (KeyError, Trade.DoesNotExist):
    return render(request, 'stocks/detail.html', {'stock': stock, 'error_message': "You didn't select a trade."})
  else:
    selected_trade.purchase_price += 1
    selected_trade.save()
    return HttpResponseRedirect(reverse('stocks:results', args=(stock.id,)))