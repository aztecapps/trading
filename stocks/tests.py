from django.test import TestCase

from .models import Stock, Trade

class TradeMethodTests(TestCase):

    def test_is_long(self):
      trade = Trade(type='Massive', purchase_price=10000, sale_price=12000)
      self.assertEqual(trade.is_long(), False)
