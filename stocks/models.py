from django.db import models

class Stock(models.Model):
  ticker = models.CharField(max_length=20)
  daily_volume = models.IntegerField(default=0)
  def __str__(self):
    return self.ticker

class Trade(models.Model):
  stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
  type = models.CharField(max_length=20)
  purchase_price = models.DecimalField(max_digits=12, decimal_places=2)
  sale_price = models.DecimalField(max_digits=12, decimal_places=2)
  def __str__(self):
    return self.type + ": " + str(self.purchase_price)
  def is_short(self):
    return self.type == "Short"
  def is_long(self):
    return self.type == "Long"
