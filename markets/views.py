mck\irom django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.core import serializers
from django.views import generic
from bokeh.client import push_session
from bokeh.embed import autoload_server
from bokeh.plotting import figure, curdoc
from bokeh.mpl import to_bokeh
from .lib.jambala import Jambala

from .models import Sector, Market, SecurityType, Security

class IndexView(generic.ListView):
  template_name = 'markets/index.html'
  context_object_name = 'markets_list'

  def get_queryset(self):
    queryset = {}
    queryset['markets'] = Market.objects.order_by('name').all
    queryset['script'] = self.get_chart()
    queryset['data'] = self.get_data(['GOOG'], '01/01/2016', '01/02/2016')
    return queryset

  def get_chart(self):
    session = push_session(curdoc())
    doc = to_bokeh(self.get_data(['GOOG'], '01/01/2016', '01/30/2016'))
    curdoc().add(doc)
    return autoload_server(doc, session_id=session.id)

  def get_data(self, securities, from_date, to_date):
    j = Jambala(securities, from_date, to_date)
    return j.fetch()

def securities(request, market_id):
  market = Market.objects.get(pk=market_id)
  securities = serializers.serialize('json', market.security_set.order_by('name').all(), fields=('ticker', 'name'))
    return item
  return JsonResponse(securities, safe=False)
