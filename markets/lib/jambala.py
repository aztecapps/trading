from datetime import date, datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class Jambala:

  url = 'http://chartapi.finance.yahoo.com/instrument/1.0/'
  securities = []
  from_date = ''
  to_date = ''
  days = ''
  datafeed = ''

  def __init__(self, securities, from_date, to_date):
    self.url = 'http://chartapi.finance.yahoo.com/instrument/1.0/'
    self.securities = securities
    self.from_date = from_date
    self.to_date = to_date
    self.set_days()
    self.build_datafeed()

  def fetch(self):
    df = pd.read_csv(self.datafeed, header=None, skiprows=32, usecols=[1,4,2,3], index_col=0, infer_datetime_format=True, parse_dates=True)
    df = df.transpose()
    df = df.drop(df.columns[range(250)], axis=1)
    plot = df.plot(kind='box')
    return plot.get_figure()


  def build_datafeed(self):
    self.datafeed = self.url + self.securities[0] + '/chartdata;type=quote;range=' + self.days + '/csv'

  def date_to_timestamp(self, d):
    return time.mktime(datetime.datetime.strptime(d, "%m/%d/%Y").timetuple())

  def set_days(self):
    days = self.get_date_range()
    self.days = str(days) + "d"

  def get_date_range(self):
    date_format = "%m/%d/%Y"
    a = datetime.strptime(self.from_date, date_format)
    b = datetime.strptime(self.to_date, date_format)
    delta = b - a
    return delta.days