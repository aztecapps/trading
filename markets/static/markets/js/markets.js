$(function() {

  $("#markets").change(function() {
    var market_id = $(this).val();
    if(market_id != "") {
      $.getJSON("/markets/" + market_id + "/securities/")
      .success(function(data) {
        data = $.parseJSON(data);
        var securities = [];
        $.each(data, function(k, v) {
            securities.push( "<option value='" + v.fields.ticker + "'>" + v.fields.name + "(" + v.fields.ticker + ")</option>" );
        })
        $('#securities').find('option').remove().end().append(securities);
        $("#securities_div").removeClass('hidden');
      });
    } else {
      $("#securities_div").addClass('hidden');
    }
  });

});

$('.datepicker').datepicker({
    autoclose: true
});