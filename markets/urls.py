from django.conf.urls import url

from . import views

app_name = 'markets'

urlpatterns = [
  url(r'^$', views.IndexView.as_view(), name='index'),
  url(r'^(?P<market_id>[0-9]+)/securities/$', views.securities, name='securities')
]