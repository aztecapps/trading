# -*- coding: utf-8 -*-
# Generated by Django 1.10.dev20160503170907 on 2016-05-04 10:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Market',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Security',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('ticker', models.CharField(max_length=20)),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='markets.Market')),
            ],
        ),
        migrations.CreateModel(
            name='SecurityType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='security',
            name='security_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='markets.SecurityType'),
        ),
        migrations.AddField(
            model_name='market',
            name='sector',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='markets.Sector'),
        ),
    ]
