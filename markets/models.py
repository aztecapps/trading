from django.db import models

class Sector(models.Model):
  name = models.CharField(max_length=50)
  def __str__(self):
    return self.name

class Market(models.Model):
  sector = models.ForeignKey(Sector, on_delete=models.CASCADE)
  name = models.CharField(max_length=50)
  def __str__(self):
    return self.name

class SecurityType(models.Model):
  name = models.CharField(max_length=50)
  def __str__(self):
    return self.name

class Security(models.Model):
  market = models.ForeignKey(Market, on_delete=models.CASCADE)
  security_type = models.ForeignKey(SecurityType, on_delete=models.CASCADE)
  name = models.CharField(max_length=100)
  ticker = models.CharField(max_length=20)
  def __str__(self):
    return self.ticker